const faker = require('faker/locale/es');
const fs = require('fs');

function generarDatos(){
    let PartSan = [];

    for(let id = 1 ; id <=500; id++){
        const id = faker.random.uuid();
        const fechaCaptura = faker.date.past();
        const expediente = faker.random.alphaNumeric(4);

        //institucionDependencia

        const institucionDependencia = faker.random.arrayElement([
            {
            "nombre": "Coordinación General Jurídica",
            "siglas": "CGJ",
            "clave": "unhr"
            },
            {
            "nombre": "Instituto de la Defensoría Pública",
            "siglas": "IDP",
            "clave": "wdfu"
            },
            {
            "nombre": "Instituto de Selección y Capacitación del Estado",
            "siglas": "ISCE",
            "clave": "mcgi"
            },
            {
            "nombre": "Jefatura de Oficina del Gobernador",
            "siglas": "JOG",
            "clave": "tdgk"
            },
            {
                "nombre": "Procuraduría General de Justicia del Estado",
                "siglas": "PGJE",
                "clave": "egsb"
            },
            {
                "nombre": "Secretaría de Administración",
                "siglas": "SA",
                "clave": "dgke"
            },
            {
                "nombre": "Secretaría de Finanzas",
                "siglas": "SF",
                "clave": "odlr"
            },
            {
                "nombre": "Secretaría de la Función Pública",
                "siglas": "SFP",
                "clave": "ldnt"
            },
            {
                "nombre": "Secretaría de Seguridad Pública",
                "siglas": "SSP",
                "clave": "peiy"
            },
            {
                "nombre": "Secretaría General de Gobierno",
                "siglas": "SGG",
                "clave": "kfhy"
            },
            {
                "nombre": "Organismo Regularizador de la Tenencia de la Tierra en Zacatecas",
                "siglas": "ORTTZ",
                "clave": "ytod"
            },
            {
                "nombre": "Patronato Estatal de Promotores Voluntarios",
                "siglas": "PEPV",
                "clave": "kdjg"
            },
            {
                "nombre": "Secretaría de Desarrollo Social",
                "siglas": "SDS",
                "clave": "aket"
            },
            {
                "nombre": "Secretaría de las Mujeres",
                "siglas": "SM",
                "clave": "ñskg"
            },
            {
                "nombre": "Secretaría del Zacatecano Migrante",
                "siglas": "SZM",
                "clave": "uroe"
            },
            {
                "nombre": "Sistema Estatal para el Desarrollo Integral de la Familia",
                "siglas": "SEDIF",
                "clave": "lsjg"
            },
            {
                "nombre": "Colegio de Bachilleres del Estado de Zacatecas",
                "siglas": "CBEZ",
                "clave": "kdjh"
            },
            {
                "nombre": "Colegio de Educación Profesional Técnica del Estado de Zacatecas",
                "siglas": "CEPTEZ",
                "clave": "lrjh"
            },
            {
                "nombre": "Colegio de Estudios Científicos y Tecnológicos del Estado de Zacatecas",
                "siglas": "CECTZ",
                "clave": "gfpt"
            },
            {
                "nombre": "Consejo Zacatecano de Ciencia, Tecnología e Innovación",
                "siglas": "CZCTI",
                "clave": "dkep"
            },
            {
                "nombre": "Escuela Estatal de Conservación y Restauración de Zacatecas",
                "siglas": "EECRZ",
                "clave": "ewcd"
            },
            {
                "nombre": "Instituto de Cultura Física y Deporte del Estado de Zacatecas",
                "siglas": "ICFDEZ",
                "clave": "jftt"
            },
            {
                "nombre": "Instituto Tecnológico Superior de Fresnillo",
                "siglas": "ITSF",
                "clave": "ldkg"
            },
            {
                "nombre": "Instituto Tecnológico Superior de Jerez",
                "siglas": "ITSJ",
                "clave": "nfot"
            },
            {
                "nombre": "Instituto Tecnológico Superior de Loreto",
                "siglas": "ITSL",
                "clave": "pejg"
            },
            {
                "nombre": "Instituto Tecnológico Superior de Nochistlán",
                "siglas": "ITSN",
                "clave": "lksd"
            },
            {
                "nombre": "Instituto Tecnológico Superior Zacatecas Norte",
                "siglas": "ITSZN",
                "clave": "lkjh"
            },
            {
                "nombre": "Instituto Tecnológico Superior Zacatecas Occidente",
                "siglas": "ITSZO",
                "clave": "uyvb"
            },
            {
                "nombre": "Instituto Tecnológico Superior Zacatecas Sur",
                "siglas": "ITSZS",
                "clave": "ieog"
            },
            {
                "nombre": "Instituto Zacatecano de Cultura",
                "siglas": "IZC",
                "clave": "añsj"
            },
            {
                "nombre": "Instituto Zacatecano de Educación para Adultos",
                "siglas": "IZEA",
                "clave": "uñjd"
            },
            {
                "nombre": "Secretaría de Educación",
                "siglas": "SE",
                "clave": "dkfb"
            },
            {
                "nombre": "Universidad Politécnica de Zacatecas",
                "siglas": "UPZ",
                "clave": "jdht"
            },
            {
                "nombre": "Universidad Politécnica del Sur de Zacatecas",
                "siglas": "UPSZ",
                "clave": "orut"
            },
            {
                "nombre": "Universidad Tecnológica del Estado de Zacatecas",
                "siglas": "UTEZ",
                "clave": "ñsjf"
            },
            {
                "nombre": "Instituto de Seguridad y Servicios Sociales para los Trabajadores del Estado",
                "siglas": "ISSSTE",
                "clave": "bvcf"
            },
            {
                "nombre": "Régimen Estatal de Protección Social en Salud ",
                "siglas": "REPSS",
                "clave": "etro"
            },
            {
                "nombre": "Servicios de Salud de Zacatecas",
                "siglas": "SSZ",
                "clave": "hfir"
            },
            {
                "nombre": "Secretaría del Campo",
                "siglas": "SC",
                "clave": "kdtt"
            },
            {
                "nombre": "Junta de Protección y Conservación de Monumentos y Zonas Típicas del Estado",
                "siglas": "JPCMZTE",
                "clave": "pwbt"
            },
            {
                "nombre": "Secretaría de Infraestructura",
                "siglas": "SI",
                "clave": "ldht"
            },
            {
                "nombre": "Secretaría del Agua y Medio Ambiente",
                "siglas": "SAMA",
                "clave": "peuty"
            },
            {
                "nombre": "Consejo Estatal de Desarrollo Económico de Zacatecas",
                "siglas": "CEDEZ",
                "clave": "mdit"
            },
            {
                "nombre": "Secretaría de Economía",
                "siglas": "SE",
                "clave": "peut"
            },
            {
                "nombre": "Secretaría de Turismo",
                "siglas": "ST",
                "clave": "ncht"
            }
        ]);


        //const nombre = faker.name.jobArea();
        //const siglas = faker.random.alphaNumeric(4);
        //const clave = faker.random.alphaNumeric(4);

        //servidorPublicoSancionado
        const rfc = faker.random.alphaNumeric(13);
        const curp = faker.random.alphaNumeric(18);
        const nombres = faker.name.firstName();
        const  primerApellido = faker.name.lastName();
        const  segundoApellido = faker.name.lastName();

        //genero
        genero = faker.random.arrayElement([
            {
                "clave": "M",
                "valor": "Masculino"
            },
            {
                "clve": "F",
                "valor": "Femenino"
            },
            {
                "clave": "O",
                "valor": "Otro"
            }
        ])
        

        const puesto = faker.random.arrayElement(["Director de área","Jefe de Departamento",
        "Oficial Federal","Director Ejecutivo","Responsable de marketing"]);;
        const nivel = faker.random.alphaNumeric(8);

        
        const autoridadSancionadora = faker.name.jobArea();

        //tipoFalta
        tipoFalta = faker.random.arrayElement([
            {
                "clave": "NAD",
                "valor": "Negligencia Administrativa"
            },
            {
                "clave": "VPC",
                "valor": "Violacion Procedimientos de COntratacion"
            },
            {
                "clave": "VLNP",
                "valor": "Violacion Leyes y Normatividad Presupuestal"
            },
            {
                "clave": "AUT",
                "valor": "Abuso de Autoridad"
            },
            {
                "clave":"CEX",
                "valor":"Cohecho o Extorsion"
            },
            {
                "clave":"IDSP",
                "valor":"Incumplimiento en Declaracion de Situacion Patrimonial"
            },
            {
                "clave":"DCSP",
                "valor":"Delito Cometido por Servidores Publicos"
            },
            {
                "clave":"EIFM",
                "valor":"Ejercicio Indebido de sus FUnciones en Materia Migratorial"
            },
            {
                "clave":"VDH",
                "valor":"Violacion a los Derechoa Humanos"
            },
            {
                "clave":"AG",
                "valor":"Administrativa Grave"
            },
            {
                "clave":"ANG",
                "valor":"Administrativa No Grave"
            },
            {
                "clave":"AC",
                "valor":"Acto de Corrupcion"
            },
            {
                "clave":"Otro",
                "valor":"Otro"
            }
        ]);
        
        
        //clave = faker.random.arrayElement(["Leve","Grave","Muy grave"]);
       //valorr = faker.random.arrayElement(["Leve","Grave","Muy grave"]);

       

        // tipoSancion
        tipoSancion = faker.random.arrayElement([
            {
                "clave": "I",
                "valor": "Inhabilitacion"
            },
            {
                "clave": "M",
                "valor": "Multado"
            },
            {
                "clave": "S",
                "valor": "Suspención de Empleo, Cargo o Comision"
            },
            {
                "clave": "D",
                "valor": "Destitución de Empleo, Cargo o Comision"
            },
            {
                "clave": "A",
                "valor": "Amolestacion"
            }
        ])

        const causaMotivoHechos = faker.random.arrayElement(["Maltrato físico","Maltrato infantil",
        "Bullying","ciberbullying","Abuso de autoridad"]);
 
        //resolucion
        const url = faker.internet.url();
        const fechaResolucion = faker.date.past();

        //multa
        const monto = faker.random.number();

        //moneda
        const moneda = faker.random.arrayElement([
            {
                "clave": "MXN",
                "valor": "Pesos Mexicanos"
            },
            {
                "clave": "USD",
                "valor": "Dolar Estadounidense"
            }
        ])

        //inhabilitacion
        const plazo = faker.random.arrayElement(["3 Meses","5 Meses","10 Meses","1 Año","5 Años","10 Años"]);
        const fechaInicial = faker.date.past();
        const fechaFinal = faker.date.future();
        

        const observaciones = faker.random.words(5);

        //documentos
        //const id = faker.random.uuid();
        const tipo = faker.random.words(2);
        const titulo = faker.name.title();
        const descripcion = faker.random.words(6);
        //const url = faker.internet.url();
        const fecha = faker.date.future();
        
        PartSan.push({
            id: id,
            fechaCaptura: fechaCaptura,
            expediente: expediente,

            institucionDependencia,
            
        servidorPublicoSancionado: {
           rfc: rfc,
           curp: curp,
           nombres: nombres,
           primerApellido: primerApellido,
           segundoApellido: segundoApellido,
           
            genero: genero,

           puesto: puesto,
           nivel: nivel
        },

        autoridadSancionadora: autoridadSancionadora,

        
            tipoFalta: tipoFalta,

        
            tipoSancion: [tipoSancion],

        causaMotivoHechos: causaMotivoHechos,

        resolucion:{
            url: url,
            fechaResolucion: fechaResolucion
        },
        multa:{
            monto: monto,
        
            moneda: moneda

     },

     inhabilitacion:{
        plazo: plazo,
        fechaInicial: fechaInicial,
        fechaFinal: fechaFinal
     },

     observaciones: observaciones,

     documentos:[{
        id: id,
         tipo: tipo,
         titulo: titulo,
         descripcion: descripcion,
        url: url,
        fecha: fecha

     }]

        })

    }

    return{result: PartSan}
}

const generatedData = generarDatos();
fs.writeFileSync('datos.json', JSON.stringify(generatedData, null, "\t"))

