const faker = require('faker/locale/es');
const fs = require('fs');

function Datos(){
    let contra = [];

    for(let id = 1 ; id <=200; id++){

        const id = faker.random.uuid();
        const fechaCaptura = faker.date.past();
        const ejercicioFiscal = faker.random.arrayElement(["2000","2001","2002","2003","2004","2005","2006",
        "2007","2008","2009","2010","2011","2012","2013","2014","2015","2016","2017","2018","2019","2020",
        "2021","2022"]);

        //periodoEjercicio
       // const fechaInicial = faker.random.arrayElement(["2000-09-27","2003-04-17","2005-02-13","2007-12-07","2008-03-20","2009-07-30"]);
       // const fechaFinal = faker.random.arrayElement(["2010-04-23","2012-11-02","2015-08-21","2013-02-10","2016-07-17","2019-12-08"]);

        //ramo
        const clave = faker.random.number({min:1, max:20});
        const valor = faker.random.words(5);

        const rfc = faker.random.alphaNumeric(13);
        const curp = faker.random.alphaNumeric(18);
        const nombres = faker.name.firstName();
        const  primerApellido = faker.name.lastName();
        const  segundoApellido = faker.name.lastName();

        //genero
        genero = faker.random.arrayElement([
            {
                "clave": "M",
                "valor": "Masculino"
            },
            {
                "clve": "F",
                "valor": "Femenino"
            },
            {
                "clave": "O",
                "valor": "Otro"
            }
        ]);

        //institucionDependencia
        const institucionDependencia = faker.random.arrayElement([
            {
            "nombre": "Coordinación General Jurídica",
            "siglas": "CGJ",
            "clave": "unhr"
            },
            {
            "nombre": "Instituto de la Defensoría Pública",
            "siglas": "IDP",
            "clave": "wdfu"
            },
            {
            "nombre": "Instituto de Selección y Capacitación del Estado",
            "siglas": "ISCE",
            "clave": "mcgi"
            },
            {
            "nombre": "Jefatura de Oficina del Gobernador",
            "siglas": "JOG",
            "clave": "tdgk"
            },
            {
                "nombre": "Procuraduría General de Justicia del Estado",
                "siglas": "PGJE",
                "clave": "egsb"
            },
            {
                "nombre": "Secretaría de Administración",
                "siglas": "SA",
                "clave": "dgke"
            },
            {
                "nombre": "Secretaría de Finanzas",
                "siglas": "SF",
                "clave": "odlr"
            },
            {
                "nombre": "Secretaría de la Función Pública",
                "siglas": "SFP",
                "clave": "ldnt"
            },
            {
                "nombre": "Secretaría de Seguridad Pública",
                "siglas": "SSP",
                "clave": "peiy"
            },
            {
                "nombre": "Secretaría General de Gobierno",
                "siglas": "SGG",
                "clave": "kfhy"
            },
            {
                "nombre": "Organismo Regularizador de la Tenencia de la Tierra en Zacatecas",
                "siglas": "ORTTZ",
                "clave": "ytod"
            },
            {
                "nombre": "Patronato Estatal de Promotores Voluntarios",
                "siglas": "PEPV",
                "clave": "kdjg"
            },
            {
                "nombre": "Secretaría de Desarrollo Social",
                "siglas": "SDS",
                "clave": "aket"
            },
            {
                "nombre": "Secretaría de las Mujeres",
                "siglas": "SM",
                "clave": "ñskg"
            },
            {
                "nombre": "Secretaría del Zacatecano Migrante",
                "siglas": "SZM",
                "clave": "uroe"
            },
            {
                "nombre": "Sistema Estatal para el Desarrollo Integral de la Familia",
                "siglas": "SEDIF",
                "clave": "lsjg"
            },
            {
                "nombre": "Colegio de Bachilleres del Estado de Zacatecas",
                "siglas": "CBEZ",
                "clave": "kdjh"
            },
            {
                "nombre": "Colegio de Educación Profesional Técnica del Estado de Zacatecas",
                "siglas": "CEPTEZ",
                "clave": "lrjh"
            },
            {
                "nombre": "Colegio de Estudios Científicos y Tecnológicos del Estado de Zacatecas",
                "siglas": "CECTZ",
                "clave": "gfpt"
            },
            {
                "nombre": "Consejo Zacatecano de Ciencia, Tecnología e Innovación",
                "siglas": "CZCTI",
                "clave": "dkep"
            },
            {
                "nombre": "Escuela Estatal de Conservación y Restauración de Zacatecas",
                "siglas": "EECRZ",
                "clave": "ewcd"
            },
            {
                "nombre": "Instituto de Cultura Física y Deporte del Estado de Zacatecas",
                "siglas": "ICFDEZ",
                "clave": "jftt"
            },
            {
                "nombre": "Instituto Tecnológico Superior de Fresnillo",
                "siglas": "ITSF",
                "clave": "ldkg"
            },
            {
                "nombre": "Instituto Tecnológico Superior de Jerez",
                "siglas": "ITSJ",
                "clave": "nfot"
            },
            {
                "nombre": "Instituto Tecnológico Superior de Loreto",
                "siglas": "ITSL",
                "clave": "pejg"
            },
            {
                "nombre": "Instituto Tecnológico Superior de Nochistlán",
                "siglas": "ITSN",
                "clave": "lksd"
            },
            {
                "nombre": "Instituto Tecnológico Superior Zacatecas Norte",
                "siglas": "ITSZN",
                "clave": "lkjh"
            },
            {
                "nombre": "Instituto Tecnológico Superior Zacatecas Occidente",
                "siglas": "ITSZO",
                "clave": "uyvb"
            },
            {
                "nombre": "Instituto Tecnológico Superior Zacatecas Sur",
                "siglas": "ITSZS",
                "clave": "ieog"
            },
            {
                "nombre": "Instituto Zacatecano de Cultura",
                "siglas": "IZC",
                "clave": "añsj"
            },
            {
                "nombre": "Instituto Zacatecano de Educación para Adultos",
                "siglas": "IZEA",
                "clave": "uñjd"
            },
            {
                "nombre": "Secretaría de Educación",
                "siglas": "SE",
                "clave": "dkfb"
            },
            {
                "nombre": "Universidad Politécnica de Zacatecas",
                "siglas": "UPZ",
                "clave": "jdht"
            },
            {
                "nombre": "Universidad Politécnica del Sur de Zacatecas",
                "siglas": "UPSZ",
                "clave": "orut"
            },
            {
                "nombre": "Universidad Tecnológica del Estado de Zacatecas",
                "siglas": "UTEZ",
                "clave": "ñsjf"
            },
            {
                "nombre": "Instituto de Seguridad y Servicios Sociales para los Trabajadores del Estado",
                "siglas": "ISSSTE",
                "clave": "bvcf"
            },
            {
                "nombre": "Régimen Estatal de Protección Social en Salud ",
                "siglas": "REPSS",
                "clave": "etro"
            },
            {
                "nombre": "Servicios de Salud de Zacatecas",
                "siglas": "SSZ",
                "clave": "hfir"
            },
            {
                "nombre": "Secretaría del Campo",
                "siglas": "SC",
                "clave": "kdtt"
            },
            {
                "nombre": "Junta de Protección y Conservación de Monumentos y Zonas Típicas del Estado",
                "siglas": "JPCMZTE",
                "clave": "pwbt"
            },
            {
                "nombre": "Secretaría de Infraestructura",
                "siglas": "SI",
                "clave": "ldht"
            },
            {
                "nombre": "Secretaría del Agua y Medio Ambiente",
                "siglas": "SAMA",
                "clave": "peuty"
            },
            {
                "nombre": "Consejo Estatal de Desarrollo Económico de Zacatecas",
                "siglas": "CEDEZ",
                "clave": "mdit"
            },
            {
                "nombre": "Secretaría de Economía",
                "siglas": "SE",
                "clave": "peut"
            },
            {
                "nombre": "Secretaría de Turismo",
                "siglas": "ST",
                "clave": "ncht"
            }
        ]);
        
        const nombre = faker.random.arrayElement(["Director de Área","Jefe de Departamentos",
        "Oficial Federal","Director Ejecutivo","Responsable de marketing","Director de Operaciones"]);
        const nivel = faker.random.alphaNumeric(10);

        //tipo area
        const tipoarea = faker.random.arrayElement([
            {"clave": "T", "valor":"TÉCNICA"},
            {"clave": "RE", "valor": "RESPONSABLE DE LA EJECUCIÓN DE LOS TRABAJOS"},
            {"clave": "RC", "valor": "RESPONSABLE DE LA CONTRATACIÓN"},
            {"clave": "O", "valor": "OTRA"},
            {"clave": "C", "valor": "CONTRATANTE"},
            {"clave": "R", "valor": "REQUIRENTE"}
        ]);

        //nivelResponsabilidad
        const nivelResponsabilidad = faker.random.arrayElement([
            {"clave": "A", "valor":"ATENCIÓN"},
            {"clave": "T", "valor":"TRAMITACIÓN"},
            {"clave": "R", "valor":"RESOLUCIÓN"},
        ]);

        //tipoProcedimiento
        const tipoProcedimiento = faker.random.arrayElement([
            {"clave": 1, "valor":"CONTRATACIONES PÚBLICAS"},
            {"clave": 2, "valor":"CONCESIONES, LICENCIAS, PERMISOS, AUTORIZACIONES Y PRÓRROGAS"},
            {"clave": 3, "valor":"ENAJENACIÓN DE BIENES MUEBLES"},
            {"clave": 4, "valor":" ASIGNACIÓN Y EMISIÓN DE DICTÁMENES DE AVALÚOS NACIONALES"}
        ])
        

        //puesto
       const puestos = faker.random.arrayElement(["Director de área","Jefe de Departamento",
        "Oficial Federal","Director Ejecutivo","Responsable de marketing"]);
       //nivel
        
       contra.push({
           id: id,
           fechaCaptura: fechaCaptura,
           ejercicioFiscal: ejercicioFiscal,

          // periodoEjercicio:{
           // fechaInicial: fechaInicial,
           // fechaFinal: fechaFinal
          // },
           ramo:{
            clave: clave,
            valor: valor
           },
           rfc: rfc,
           curp: curp,
           nombres: nombres, 
           primerApellido: primerApellido,
           segundoApellido: segundoApellido,
        
           genero: genero,

           institucionDependencia: institucionDependencia,

           puesto: {
            nombre:nombre,
            nivel: nivel
           },
           tipoArea:[tipoarea],
           nivelResponsabilidad: [nivelResponsabilidad],
           tipoProcedimiento:[tipoProcedimiento],
           superiorInmediato: {
            nombres: nombres,
            primerApellido: primerApellido,
            segundoApellido: segundoApellido,
            curp: curp,
            rfc:rfc,
            puesto:{
                nombre: nombre,
                nivel:nivel
               }
           }
           

       })

    }

    return{result: contra}
}

const generatedData = Datos();
fs.writeFileSync('datoscon.json', JSON.stringify(generatedData, null, "\t"))